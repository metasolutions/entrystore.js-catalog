define([
    'dojo/json',
    'rdfjson/print',
    'store/EntryStore',
    'rdfjson/namespaces',
    'config',
    '../utils/message'
], function(json, print, EntryStore, namespaces, config, message) {
    //browsers have the global nodeunit already available

    var es = config.entrystore = new EntryStore(config.repository);
    var auth = es.getAuth();
    namespaces.add("dcat", "http://www.w3.org/ns/dcat#");

    auth.login(config.user, config.password).then(function (data) {
        var ctx, eid;
        if (process.argv.length > 3) {
            ctx = process.argv[3];
        }

        if (process.argv.length > 4) {
            eid = process.argv[4];
        }

        if (ctx == null || eid == null) {
            message("You need to specify both a contextid and an entryid.");
        } else {
            var da;
            es.getEntry(es.getEntryURI(ctx, eid)).then(function (entry) {
                message(entry.getMetadata());
            });
        }
    });
});