define([
    'dojo/_base/array',
    'dojo/promise/all',
    'store/EntryStore',
    'store/solr',
    'rdfjson/namespaces',
    'config',
    '../utils/GCEList',
    '../utils/message'
], function(array, all, EntryStore, solr, namespaces, config, GCEList, message) {
    //browsers have the global nodeunit already available

    var es = config.entrystore = new EntryStore(config.repository);
    var auth = es.getAuth();
    namespaces.add("dcat", "http://www.w3.org/ns/dcat#");

    auth.login(config.user, config.password).then(function (data) {

        var ctx, dst;
        if (process.argv.length > 3) {
            ctx = process.argv[3];
        }
        if (process.argv.length > 4) {
            dst = process.argv[4];
        }

        //Show catalogs
        if (ctx == null) {
            auth.getUserEntry().then(function (userEntry) {
                var list = new GCEList({userEntry: userEntry, entryId: "dcat"});
                list.getEntries(0).then(function (arr) {
                    if (arr.length > 0) {
                        var arr = array.map(arr, function (e) {
                            var title = e.getMetadata().findFirstValue(null, "dcterms:title");
                            return "Id: " + e.getContext().getId() + "\t\t\tTitle: " + title;
                        });
                        message("Catalogs available for user: " + data.user, arr);
                    }
                });
            });
        } else {
            es.getEntry(es.getEntryURI(ctx, "dcat")).then(function (dcat) {
                var ctitle = dcat.getMetadata().findFirstValue(null, "dcterms:title");
                if (dst == null) { //Show datasets in a catalog
                    es.createSearchList(solr.rdfType(namespaces.expand("dcat:Dataset"))
                        .context(es.getContextById(ctx).getResourceURI())
                        .limit(50)).getEntries(0).then(function (arr) {
                        var mesg;
                        if (arr.length > 0) {
                            mesg = array.map(arr, function (e) {
                                var title = e.getMetadata().findFirstValue(null, "dcterms:title");
                                return "Id: " + e.getId() + "\t\t\tTitle: " + title;
                            });
                        } else {
                            mesg = "No datasets found.";
                        }
                        message("Catalog \"" + ctitle + "\" contains the following datasets:", mesg);
                    });
                } else { //Show distributions in a dataset
                    var str1;
                    es.getEntry(es.getEntryURI(ctx, dst)).then(function (dataset) {
                        var md = dataset.getMetadata();
                        var dtitle = md.findFirstValue(null, "dcterms:title");
                        str1 = "In catalog \"" + ctitle + "\".\n | Dataset \""
                        + dtitle + "\" contains the following distributions:";

                        var distributionStmts = md.find(null, "dcat:distribution");
                        var arrOfDistPromises = array.map(distributionStmts, function(stmt) {
                            //Object of statement is the resource URI of the entry.
                            //We need to get the entryURI to be able to get it.
                            var entryURI = es.getEntryURIFromURI(stmt.getValue());
                            return es.getEntry(entryURI);
                        });
                        //Return a single promise when all promises are realized (or one failed)
                        return all(arrOfDistPromises);
                    }).then(function(dists) {
                        var str2;
                        if (dists.length > 0) {
                            str2 = array.map(dists, function (e) {
                                var md = e.getMetadata();
                                var ruri = e.getResourceURI();
                                var dititle = md.findFirstValue(ruri, "dcterms:title")
                                    || md.findFirstValue(ruri, "dcat:accessURL")
                                    || md.findFirstValue(ruri, "dcat:downloadURL");
                                return "Id: " + e.getId() + "\t\t\tTitle: " + dititle;
                            });
                        } else {
                            str2 = "No distributions found.";
                        }
                        message(str1, str2);
                    });
                }
            });
        }
    });
});