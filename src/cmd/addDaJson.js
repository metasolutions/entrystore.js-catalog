define([
    'store/EntryStore',
    'rdfjson/namespaces',
    'config',
    '../utils/message',
    'dojo/node!fs'
], function(EntryStore, namespaces, config, message, fs) {
    //browsers have the global nodeunit already available

    var es = config.entrystore = new EntryStore(config.repository);
    var auth = es.getAuth();
    namespaces.add("dcat", "http://www.w3.org/ns/dcat#");

    auth.login(config.user, config.password).then(function (data) {
        var ctx, inputData;
        var jsonAdd = function(subj, md) {
            for (var key in inputData) {
                md.add(subj, key, inputData[key]);
            }
        };

        var f = function () {
            if (ctx == null) {
                message([
                    "You need to specify which catalog to add the dataset to, ",
                    "use 'ls' to find existing catalogs you have access to."]);
            } else {
                var da;
                es.getEntry(es.getEntryURI(ctx, "dcat")).then(function (dcat) {
                    var pe = dcat.getContext().newEntry();
                    var md = pe.getMetadata();
                    var ruri = pe.getResourceURI();
                    if (inputData != null) {
                        jsonAdd(ruri, md);
                    } else {
                        md.addL(ruri, "dcterms:title", "New dataset " + (new Date()));
                    }
                    md.add(ruri, "rdf:type", "dcat:Dataset");
                    return pe.commit().then(function (newDa) {
                        da = newDa;
                        dcat.getMetadata().add(dcat.getResourceURI(),
                            "dcat:dataset", newDa.getResourceURI());
                        return dcat.commitMetadata();
                    });
                }).then(function () {
                    message("Created dataset with id: " + da.getId());
                }), function (err) {
                    message("Failed creating dataset: " + err);
                };
            }
        };

        if (process.argv.length > 3) {
            ctx = process.argv[3];
        }

        if (process.argv.length > 4) {
            var filepath = process.argv[4];
            fs.readFile(filepath, 'utf8', function (err, data) {
                inputData = JSON.parse(data);
                f();
                //graph = converters.rdfxml2graph(data);
            });
        } else {
            f();
        }
    });
});