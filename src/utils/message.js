define([
    "dojo/_base/array",
    "dojo/_base/lang",
    "rdfjson/Graph",
    "rdfjson/namespaces"
], function(array, lang, Graph, namespaces) {
    var divider = function(pre, post) {
        console.log(
            (pre ? "\n": "")+
            "  -------------------------------------------------------------"+
            (post ? "\n" : "")
        );
    };

    var mesg = function(mesg) {
        console.log(" | "+mesg);
    }

    return function(str1, str2) {
        divider(true);
        if (str1 instanceof Graph) {
            array.forEach(str1.find(), function(stmt) {
                mesg("< "+namespaces.shorten(stmt.getSubject())+", "
                +namespaces.shorten(stmt.getPredicate())+", "+
                (stmt.getType() === "uri" ? namespaces.shorten(stmt.getValue()) : "\""+stmt.getValue()+"\"")+">");
            });
        }
        if (typeof str2 === "undefined") {
            array.forEach(lang.isArray(str1) ? str1 : [str1], mesg);
        } else {
            array.forEach(lang.isArray(str1) ? str1 : [str1], mesg);
            divider();
            array.forEach(lang.isArray(str2) ? str2 : [str2], mesg);
        }
        divider(false, true);
    };
});