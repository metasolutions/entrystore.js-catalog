define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/promise/all",
    "dojo/_base/lang",
    "./ArrayList",
    "config"
], function (declare, array, all, lang, ArrayList, config) {

    /**
     * A Group-Context-Entry list (GCEList) provides a list of entries corresponding to objects
     * (like data-catalogs and conceptschemes) maintained in separate contexts.
     * Keeping the objects (and all belonging entries) in separate context provides
     * good maintainability and access control. A separate group is also defined per context
     * to allow collaboration. The current user may have access to zero or more of
     * these objects which is detected by checking memberships of groups, checking
     * their home-contexts and then for a corresponding entry in each context.
     */
    return declare(ArrayList, {
        constructor: function(params) {
            this.userEntry = params.userEntry;
            this.entryId = params.entryId;
            this.contextId2groupId = {};
        },
        setGroupIdForContext: function(contextId, groupId) {
            this.contextId2groupId[contextId] = groupId;
        },
        getGroupId: function(contextId) {
            return this.contextId2groupId[contextId];
        },

        loadEntries: function() {
            var es = config.entrystore,
                groupURIs = this.userEntry.getParentGroups();
            return all(array.map(groupURIs, function (guri) {
                return es.getEntry(guri);
            })).then(lang.hitch(this, this.extractEntriesFromGroups))
                .then(lang.hitch(this, function(entries) {
                    this.entries = entries;
                    return entries;
                }));
        },

        extractEntriesFromGroups: function(groupEntryArr) {
            var es = config.entrystore,
                hcArr = array.map(groupEntryArr, function (ge) {
                    return ge.getResource(true).getHomeContext();
                });

            var entries = [], entryDefs = [];
            array.forEach(hcArr, function (hc, idx) {
                if (hc != null) {
                    this.setGroupIdForContext(hc, groupEntryArr[idx].getId());
                    entryDefs.push(es.getEntry(es.getEntryURI(hc, this.entryId)).then(function (entry) {
                        entries[idx] = entry;
                    }, function () {
                        entries[idx] = null;
                    }));
                } else {
                    entries[idx] = null;
                }
            }, this);
            return all(entryDefs).then(function () {
                return array.filter(entries, function (entry) {
                    return entry !== null;
                });
            });
        }
    });
});