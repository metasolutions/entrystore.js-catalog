define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojo/_base/lang"
], function (declare, Deferred, lang) {

    /**
     * Wraps an array (possibly asynchrounous loaded) into a class that looks like a store/List.
     * @see {store/List}
     */
    return declare(null, {
        limit: 20,

        /**
         * Parameters may be:
         * arr - an array of entries to be used,
         *      if not provided the loadEntries method must be overrided
         * limit - an integer specifying the page limit.
         *
         * @param {object} params
         */
        constructor: function(params) {
            this.entries = params.arr;
            this.limit = params.limit != null ? params.limit : this.limit;
        },


        /**
         * Resets the cached entries, if used the loadEntries method should be properly implemented.
         */
        refresh: function() {
            delete this.entries;
        },

        /**
         * Implement this method if asynchronous loading of entries is needed.
         * Make sure to store the array of entries in this.entries before the returned promise is resolved.
         *
         * @return {Promise}
         */
        loadEntries: function() {
        },

        getEntries: function(page) {
            if (this.entries == null) {
                return this.loadEntries().then(lang.hitch(this, this.getEntries, page));
            } else {
                var d = new Deferred();
                d.resolve(this.entries.slice(page*this.getLimit(), (page+1)*this.getLimit()));
                return d;
            }
        },

        getLimit: function() {
            return this.limit;
        },

        getSize: function() {
            return this.entries == null ? -1 : this.entries.length;
        }
    });
});